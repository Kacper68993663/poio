#include <iostream>
#include <string>
#include <sstream>>

using namespace std;
//////////////////////////////////////
class Packet
{
protected:
	string device;
	string description;
	long unsigned int date;

	//public:
	//virutal void puste() =0;
};
/////////////////////////////////////
class Sequence :public Packet
{
protected:
	int channelNr;
	string unit;
	double resolution;
	//buffer jeszcze

};									//ASBTRA
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
class TimeHistory :public Sequence	//KONKRET
{
	double sensitivity;
public:
	TimeHistory(string dev, string descrip, long unsigned int data, int channel, string un, double res, double sensi) //konstruktor
	{
		device = dev;
		description = descrip;
		date = data;
		channelNr = channel;
		unit = un;
		resolution = res;
		sensitivity = sensi;
	}
	void wypisz()
	{
		string lancuch_danych = device + "\n" + description + "\n" + to_string(date) +"\n" + to_string(channelNr) + "\n" +unit + "\n" +to_string(resolution) + "\n" +to_string(sensitivity);
		cout << lancuch_danych<<endl;
	}


};
///////////////////////////////////////////////////////////
class Spectrum :public Sequence		//KONKRET
{
	int scaling; //jesli scaling =0 to skala jest liniowa, jesli =1 to logarytmiczna	
public:
	Spectrum(string dev, string descrip, long unsigned int data, int channel, string un, double res, int scal)
	{
		device = dev;
		description = descrip;
		date = data;
		channelNr = channel;
		unit = un;
		resolution = res;
		scaling = scal;

	}
	void wypisz()
	{
		string lancuch_danych = device + "\n" + description + "\n" + to_string(date) + "\n" + to_string(channelNr) + "\n" + unit + "\n" + to_string(resolution) + "\n" + to_string(scaling);
		cout << lancuch_danych<<endl;
	}
};
//////////////////////////////////////////////////////////////////
class Alarm :public Packet			//KONKRET
{
	int channelNr;
	int treshold;
	int direction; //0 - dowolny, -1 w dol, +1 w gore
public:
	Alarm(string dev, string desc, long unsigned int data, int channel, int tresh, int dir)
	{
		device = dev;
		description = desc;
		date = data;
		channelNr = channel;
		treshold = tresh;
		direction = dir;
	}

	void wypisz()
	{
		string lancuch_danych = device + "\n" + description + "\n" + to_string(date) + "\n" + to_string(channelNr) + "\n" + to_string(treshold) + "\n" + to_string(direction);
		cout << lancuch_danych<<endl;
	}
};

int main()
{

	Alarm test("jeden", "dwa", 3, 4, 5, 0); // Device NAZWA|opis|czas od poczatku|kanal| treshold|kierunek
	test.wypisz();
	Spectrum test1("nie", "wiem", 3, 7, "qwe", 10, 19); //Device NAZWA|opis|czas od poczatku|kanal|jednostka|resolution|scaling
	//test1.wypisz();
	TimeHistory test2("jakis", "tam", 3, 0, "test", 20, 35); //Device NAZWA|opis|czas od poczatku|kanal|jednostka|resolution|sensitivity
	//test2.wypisz();
	

	return 0;
}