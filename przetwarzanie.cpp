#include <iostream>
#include <chrono>
#include <random>
#include <math.h>
#include "ffft/FFTReal.h"
#include <fstream>
using namespace std;
const int czas = 10;
const int probkowanie = 400;



class Sygnal{


public: //protected:		// PUBLIC: W CELU SPRAWDZENIA POPRAWNOSCI DZIALANIA W FUNKCJI MAIN, NORMALNIE POWINNO BYC PROTECTED
	long double data[czas * probkowanie];
public:
	
	Sygnal(float mean, double  stddev) {	//ZADANIE 1 ORAZ 2		// rozklad gaussa i zapisanie jego wynikow do tablicy data

		float* srednia = new float;
		double* odchylenie = new double;
		*srednia = mean, *odchylenie = stddev;

		unsigned seed = chrono::system_clock::now().time_since_epoch().count();
		default_random_engine generator(seed);
		normal_distribution<double> distribution(*srednia, *odchylenie);

		for (int i = 0; i < czas * probkowanie; i++) data[i] = distribution(generator);
		//cout <<data[czas * probkowanie-1]<<endl; // sprawdzenie

		delete srednia, odchylenie;
	}					
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void wypiszRMS() {			//ZADANIE  8 
		long double* suma_x = new long double;
		long double* u_x = new long double;
		long double* mean = new long double; 
		long double RMS;
		*suma_x = 0, *mean = 0;
		for (unsigned int i = 1; i <= czas * probkowanie; i++) *suma_x += data[i - 1];
		*u_x = *suma_x / (czas * probkowanie);
		for (unsigned int i = 1; i <= czas * probkowanie; i++) *mean += pow((data[i - 1] - *u_x), 2);
		RMS = sqrt(*mean / (czas * probkowanie - 1));
		cout << RMS << endl;
		delete suma_x, u_x, mean;
	}					
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	void operator == (Sygnal & kanal) {		//ZADANIE  3
		
		long double  *buffer = new long double[czas * probkowanie];
		for (unsigned int i = 0; i < czas * probkowanie; i++) {
			buffer[i] = data[i];
			data[i] = kanal.data[i];
			kanal.data[i] = buffer[i];
			
		}
		delete buffer;
	
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	long double* operator+(Sygnal& kanal) {		//ZADANIE 4
		long double *wynik_dodawania= new long double[czas * probkowanie];
		for (unsigned int i = 0; i < czas * probkowanie; i++) wynik_dodawania[i] = data[i] + kanal.data[i];
		
		return wynik_dodawania;
		delete wynik_dodawania;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	long double* operator/(Sygnal& kanal) {		//ZADANIE 5
		long double* wynik_dzielenia = new long double[czas * probkowanie];
		for (unsigned int i = 0; i < czas * probkowanie; i++) wynik_dzielenia[i] = data[i] / kanal.data[i];
		
		
		return wynik_dzielenia;
		delete wynik_dzielenia;
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	friend void FFT();
	
	
};
//KONIEC KLASY
/////////////////////////////////////////
//PONIZEJ FUNKCJA 
void FFT(Sygnal kanal1, Sygnal kanal2, Sygnal kanal3, Sygnal kanal4) {

	const long len = 4096;
	ffft::FFTReal <long double> fft_object(len);
	long double* x1 = new long double[len];  long double* x2 = new long double[len];	long double* x3 = new long double[len]; long double* x4 = new long double[len];
	long double* f1 = new long double[len]; long double* f2 = new long double[len];  long double* f3 = new long double[len]; long double* f4 = new long double[len];
	long double* modul1 = new long double[len]; long double* modul2 = new long double[len]; long double* modul3 = new long double[len]; long double* modul4 = new long double[len];
	
	for (unsigned int i = 0; i < czas * probkowanie; i++) {
		x1[i] = kanal1.data[i], x2[i] = kanal2.data[i], x3[i] = kanal3.data[i], x4[i] = kanal4.data[i];		
	}
	for (unsigned i = czas * probkowanie; i < len; i++) {	//tablica sygnalow ma 4000 elementow, a tablica ktora wchodzi do funkcji fft musi miec dlugosc 4096 albo inna potege 2,brakujace pola uzupelniam wartoscia ostatniej komorki sygnalu
		x1[i] = kanal1.data[czas * probkowanie - 1], x2[i] = kanal2.data[czas * probkowanie - 1], x3[i] = kanal3.data[czas * probkowanie - 1], x4[i] = kanal4.data[czas * probkowanie - 1];
	}
	
	fft_object.do_fft(f1, x1);
	fft_object.do_fft(f2, x2);
	fft_object.do_fft(f3, x3);
	fft_object.do_fft(f4, x4);
	for (unsigned int i = 0; i < len; i++) {
		modul1[i] = abs(f1[i]), modul2[i] = abs(f2[i]), modul3[i] = abs(f3[i]), modul4[i] = abs(f4[i]);			// mamy wszystkie potrzebne rzeczy, teraz wystarczy zapisac do plikow "data", "f" oraz "modul"
	}
	
	fstream sygnal_1, sygnal_2, sygnal_3, sygnal_4, zespolone1, zespolone2, zespolone3, zespolone4, amplitudowe1, amplitudowe2, amplitudowe3, amplitudowe4;
	sygnal_1.open("sygnal1.txt", ios::out), sygnal_2.open("sygnal2.txt", ios::out), sygnal_3.open("sygnal3.txt", ios::out),sygnal_4.open("sygnal4.txt", ios::out),
		zespolone1.open("zespolone1.txt", ios::out), zespolone2.open("zespolone2.txt", ios::out), zespolone3.open("zespolone3.txt", ios::out), zespolone4.open("zespolone4.txt", ios::out),
		amplitudowe1.open("amplitudowe1.txt", ios::out), amplitudowe2.open("amplitudowe2.txt", ios::out), amplitudowe3.open("amplitudowe3.txt", ios::out), amplitudowe4.open("amplitudowe4.txt", ios::out);
	for (unsigned i = 0; i < len; i++) {
		sygnal_1 << x1[i] << endl;
		sygnal_2 << x2[i] << endl;
		sygnal_3 << x3[i] << endl;
		sygnal_4 << x4[i] << endl;
		zespolone1 << f1[i] << endl;
		zespolone2 << f2[i] << endl;
		zespolone3 << f3[i] << endl;
		zespolone4 << f4[i] << endl;
		amplitudowe1 << modul1[i] << endl;
		amplitudowe2 << modul2[i] << endl;
		amplitudowe3 << modul3[i] << endl;
		amplitudowe4 << modul4[i] << endl;
	}
	sygnal_1.close(), sygnal_2.close(), sygnal_3.close(), sygnal_4.close(), zespolone1.close(), zespolone2.close(), zespolone3.close(), zespolone4.close(), amplitudowe1.close(), amplitudowe2.close(), amplitudowe3.close(), amplitudowe4.close();
	delete x1, x2, x3, x4, f1, f2, f3, f4, modul1, modul2, modul3, modul4;

}
//////////////////////////////
//MAINIK
//////////////////////////////

int main() {

	//PUNKT 1 i 2
	Sygnal sygnal1(3, 0.3);
	Sygnal sygnal2(6, 0.3);
	Sygnal sygnal3(8, 0.1);
	Sygnal sygnal4(3, 0.3);
	
	//PUNKT 3
	cout << "ZAMIANA KANALAOW" << endl;
	cout << sygnal1.data[1] << "	" << sygnal2.data[1] << endl;	//sprawdzenie zamiany
	sygnal1 == sygnal2;												// od teraz obiekt sygnal1 przechowuje sygnal 6V, a obiekt sygnal2 przechowuje sygnal 3V 
	cout << sygnal1.data[1] << "	" << sygnal2.data[1] << endl;	//sprawdzenie zamiany

	//PUNKT 4
	cout << "\nDODAWANIE KANALAOW" << endl;
	cout << sygnal2.data[1] << "	" << sygnal3.data[1] << endl;	//sprawdzenie dodawania
	cout<<(sygnal2 + sygnal3)[1]<<endl;								//sprawdzenie dodawania
	
	
	//PUNKT 5
	cout << "\nDZIELENIE KANALOW" << endl;
	cout << sygnal1.data[1] << "	" << sygnal4.data[1] << endl;	//sprawdzenie dzielenia
	cout << (sygnal1 / sygnal4)[1] << endl;							//sprawdzenie dzielenia

	//PUNKT 6 I 7

	FFT(sygnal1,sygnal2,sygnal3,sygnal4);		//wyznaczenie FFT i zapis do pliku

	//PUNKT 8
	cout << "\nOdchylenie standardowe sygnalu nr 1:	";
	sygnal1.wypiszRMS();
	cout << "Odchylenie standardowe sygnalu nr 2:	";
	sygnal2.wypiszRMS();
	cout << "Odchylenie standardowe sygnalu nr 3:	";
	sygnal3.wypiszRMS();
	cout << "Odchylenie standardowe sygnalu nr 4:	";
	sygnal4.wypiszRMS();

	return 0;
}



					